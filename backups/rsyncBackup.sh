#!/usr/bin/bash

if [ $# -ne  2 ] ; then
  (>&2 echo "Usage : $0 <source> <dest>")
  exit
fi


curDate=`date +%Y-%m-%d-%H:%M:%S`

srcDir=`readlink -f $1`
destDir=`readlink -f $2`

if ! [ -d $srcDir -a -r $srcDir -a -x $srcDir ] ; then 
  (>&2 echo "Usage : $0 <source> <dest>")
  (>&2 echo "<source> must be a readable directory. $srcDir is not")
  exit
fi


if ! [ -d $destDir -a -w $destDir ] ; then 
  (>&2 echo "Usage : $0 <source> <dest>")
  (>&2 echo "<dest> must be a writable directory. $destDir is not")
  exit
fi

prevBackup=`ls -1t --time=birth $destDir| head -n 1`

if [ -z $prevBackup ] ; then

  # No previous backup found in dir : make a standard copy
  echo 'no prev backup, running rsync -av'
  echo "rsync -av ${srcDir}/ ${destDir}/${curDate}"
  rsync -av ${srcDir}/ ${destDir}/${curDate}

else

  #prev backup found : use hardlink whenever possible
  echo "Previous backup found $prevBackup";
  echo "rsync -av --link-dest=${destDir}/${prevBackup} ${srcDir}/ ${destDir}/${curDate}"
  rsync -av --link-dest=${destDir}/${prevBackup} ${srcDir}/ ${destDir}/${curDate}

fi;


################
# Remove older backups
#
# Older backups may be removed with the following
# find ${destDir}/ -mindepth 1 -maxdepth 1 -ctime +${maxDaysToKeep} -exec rm -rf {} +
# where maxDaysToKeep is an integer
################
